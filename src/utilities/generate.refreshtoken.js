const jwt = require("jsonwebtoken");

const generateRefreshToken = (id) => {
  return jwt.sign({ userId: id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: process.env.REFRESH_TOKEN_EXPIRED,
  });
};

module.exports = generateRefreshToken;
