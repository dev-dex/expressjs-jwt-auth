const { User } = require("../models");
const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");

const CreateNewUser = asyncHandler(async (req, res) => {
  const user = await User.findOne({
    where: {
      user_email: req.body.email,
    },
  });

  if (user) {
    res.status(409);
    throw new Error("Email Address is already taken");
  }

  const hashPass = await bcrypt.hash(req.body.password, 10);

  const newUser = await User.create({
    user_name: req.body.name,
    user_email: req.body.email,
    user_password: hashPass,
  });

  res.status(201).json({
    message: "Registered Successfully",
    data: newUser,
  });
});

const GetAuthorizeUser = asyncHandler(async (req, res) => {
  const id = req.userId;

  const user = await User.findByPk(id);

  res.status(200).json({
    message: "This user is Authorize",
    data: user,
  });
});

module.exports = { CreateNewUser, GetAuthorizeUser };
