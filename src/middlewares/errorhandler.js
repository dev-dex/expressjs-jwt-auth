/* eslint-disable no-unused-vars */
const ErrorHandler = (err, req, res, next) => {
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;

  res.status(statusCode);
  res.json({
    message: err.message,
    stacks: process.env.NODE_ENV === "production" ? null : err.stack,
  });
};

module.exports = ErrorHandler;
